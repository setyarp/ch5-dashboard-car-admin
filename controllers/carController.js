const imagekit = require("../lib/imageKit");
const { cars } = require("../models");

// get atau retrieve function controller
async function getCars(req, res) {
    try {
        const responseData = await cars.findAll()
        res.status(200).json({
            'test': '123 sayang semuanya',
            'data': responseData
        })
    } catch (err) {
        console.log(err.message)
    }
}

// create new data cars
async function createCar(req, res) {
    try {
        // process file naming        
        const split = req.file.originalname.split('.')
        console.log(split)
        const extension = split[split.length - 1]
        console.log(extension)

        const imageName = req.file.originalname + '.' + extension
        console.log(imageName)

        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })

        console.log(img)

        const name = req.body.name
        const rent = req.body.rent
        const size = req.body.size
        // const { name, size } = req.body

        const newCar = await cars.create({ 
            name: name,
            imageUrl: img.url,
            rent: rent,
            size: size,
        })
        
        res.status(200).json({
            'status': 'success',
            'data': newCar
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

// update data cars
async function updateCar(req, res) {
    try {
        const split = req.file.originalname.split('.')
        const extension = split[split.length - 1]
        const imageName = req.file.originalname + '.' + extension
        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })

        const name = req.body.name
        const rent = req.body.rent
        const size = req.body.size
        
        const imageUrl = img.url
        const id = req.params.id
        const responseData = await cars.update({name,rent, size, imageUrl}, {
            where: {id}
        })
        res.status(200).json({
            'success': true
        })
    } catch (err) {
        console.log(err.message)
    }
}

// delete data
async function deleteCar (req, res){
    try {
        const id = req.params.id
        const responseData = await cars.destroy({where: {id}})
        res.status(200).json({
            'success': true
        })
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = {
    getCars,
    createCar,
    updateCar,
    deleteCar,
}