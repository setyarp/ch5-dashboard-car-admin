const imagekit = require("../../lib/imageKit");
const { Op, and } = require("sequelize");
const { cars } = require("../../models");

// Home Page 
async function getCars(req, res) {
    const items = await cars.findAll()
    res.render("index", {
        items,
    });
};

// Create Data Cars Page 
async function createCarPage(req, res) {
    res.render("create");
}

// Create Data Cars
async function createCar(req, res) {
    const imageName = req.file.originalname
    // upload file 
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: imageName,
    })
    const imageUrl = img.url
    const { name, rent, size } = req.body
    await cars.create({ 
        name: name,
        rent: rent,
        imageUrl: imageUrl,
        size: size,
    })
    res.redirect("/")
};

// Update Data Car Page
async function updateCarPage(req, res) {
    const id = req.params.id;
    const items = await cars.findByPk(id)
    res.render("update", {
        items,
    });
}

// Update Data Cars
async function updateCar(req, res) {
    const id = req.params.id;
    const imageName = req.file.originalname
    // upload file 
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: imageName,
    })
    const imageUrl = img.url
    const { name, rent, size } = req.body
    await cars.update({ 
        name: name,
        rent: rent,
        imageUrl: imageUrl,
        size: size,
    },
    {
        where:{id},
    })
    res.redirect("/")
};

// Delete Data Cars
async function deleteCar(req, res) {
    const id = req.params.id;
    await cars.destroy({ where: { id } });
    res.redirect("/");
  }
  

// search page
async function searchCars(req, res) {
    let items
    items = await cars.findAll({
        where: {
            [Op.or]: [
                // { name: req.query.name },
                { size: req.query.size }
            ]
        }
    })
    res.render("index", {
        items,
    });
};

// search page
async function searchCarsname(req, res) {
    let items
    items = await cars.findAll({
        where: {
            [Op.and]: [
                { name: req.query.name },
            ]
        }
    })
    res.render("index", {
        items,
    });
};

module.exports = {
    getCars,
    createCar,
    updateCar,
    createCarPage,
    updateCarPage,
    deleteCar,
    searchCars,
    searchCarsname
}