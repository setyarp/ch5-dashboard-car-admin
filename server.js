// Import env
const dotenv = require('dotenv')
dotenv.config()

// Import express module
const express = require("express")
const bodyParser = require("body-parser")
const path = require("path")

// our own module
const routes = require("./routes")

// Initializations
const app = express();
const port = 3000;

// Static files untuk menampilkan css dan image
app.use(express.static("public"));
app.use(express.static("controllers"));

// Setting up view engine ejs
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.json())

// Routes
app.use(routes)

// Running server
app.listen(port, () => {
  console.log(`Server berjalan pada ${Date(Date.now)}`)
  console.log(`Sever sudah berjalan di http://localhost:${port}`);
});
