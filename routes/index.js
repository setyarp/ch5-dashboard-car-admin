const router = require("express").Router();

const carController = require('../controllers/carController')
const adminCarController = require('../controllers/admin/carController')

const uploader = require('../middleware/uploader')

// Routes API
router.get('/api/cars', carController.getCars)
router.post('/api/cars', uploader.single('image'), carController.createCar)
router.put('/api/cars/:id',uploader.single('image'), carController.updateCar)
router.delete('/api/cars/:id', carController.deleteCar)


// Routes Dashboard Admin
router.get('/', adminCarController.getCars)
router.post('/admin/add', uploader.single('image'), adminCarController.createCar)
router.post('/admin/edit/:id', uploader.single('image'), adminCarController.updateCar)
router.get('/admin/delete/:id', adminCarController.deleteCar)
router.get('/search', adminCarController.searchCars)
router.get('/searchbyname', adminCarController.searchCarsname)

// Routes Admin page
router.get('/admin/add', adminCarController.createCarPage)
router.get('/admin/edit/:id', adminCarController.updateCarPage)

module.exports = router